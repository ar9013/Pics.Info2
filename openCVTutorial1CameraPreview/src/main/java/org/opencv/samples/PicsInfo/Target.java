package org.opencv.samples.PicsInfo;

import android.location.Location;
import android.widget.Toast;

import java.util.LinkedList;

/**
 * Created by kangyue on 2/11/18.
 */

public class Target {

    private Location location = null;
    private String name = null;



    public Target(String provider, double lat , double lng, String name ){

        this.name = name ;
        location = new Location(provider);
        this.location.setLatitude(lat);
        this.location.setLongitude(lng);


    }

    Location getLocation(){
        return location;
    }

    String getName(){
        return name;
    }



}
