package org.opencv.samples.PicsInfo;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.SurfaceView;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;

import java.util.LinkedList;

public class GpsActivity extends Activity implements CameraBridgeViewBase.CvCameraViewListener2 {

    private static final String TAG = "GpsActivity";
    private static final int MY_PERMISSIONS_REQUEST_OPEN_CAMERA = 1;
    private ARActivity activity;
    private CameraBridgeViewBase mOpenCvCameraView;
    private boolean  mIsJavaCamera = true;
    private Camera camera;

    private TextView locationTitle,locationDisp;
    private GpsTool gpsTool;

    private String provider = null;

    private LinkedList<Target> targetList = null;
    private LinkedList<Double> distanceList = null;

    double  mini = 0; // 存放 最小距離
    int nid = 0;   // 紀錄 靠近的那項

    private BaseLoaderCallback loaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            super.onManagerConnected(status);
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }

        }
    };


        @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_gps);

        targetList = new LinkedList<Target>();
        distanceList = new LinkedList<Double>();

            mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.tutorial1_activity_java_surface_view);

            mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);

            mOpenCvCameraView.setCvCameraViewListener(this);

            setTargetList();

        locationTitle = (TextView) findViewById(R.id.vLOcationTitle);

        locationDisp = (TextView) this.findViewById(R.id.vLocationDisp);
        if (gpsTool == null) {
            gpsTool = new GpsTool(this) {
                @Override
                public void onGpsLocationChanged(Location location) {
                    super.onGpsLocationChanged(location);
                    refreshLocation(location);
                }
            };
        }
    }

    private void setTargetList(){


            Target target1 = new Target(provider,25.058696,121.613573,"方螞科技");
            Target target2 = new Target(provider,25.057274,121.614586,"玉山銀行");
            Target target3 = new Target(provider,25.056660,121.611903,"郵局");
            Target target4 = new Target(provider,25.057348,121.613587,"全家便利商店");
            Target target5 = new Target(provider,25.058190,121.613250,"星巴克");
            Target target6 = new Target(provider,25.056997,121.613605,"仮屋");
            Target target7 = new Target(provider,25.057359,121.613358,"KKBox");
            Target target8 = new Target(provider,25.057134,121.613580,"富邦");
            Target target9 = new Target(provider,25.059112,121.613375,"三米便當屋");
            Target target10 = new Target(provider,25.058827,121.612999,"合作金庫");

            targetList.add(target1);
            targetList.add(target2);
            targetList.add(target3);
            targetList.add(target4);
            targetList.add(target5);
            targetList.add(target6);
            targetList.add(target7);
            targetList.add(target8);
            targetList.add(target9);
            targetList.add(target10);

    }

    private void refreshLocation(Location location) {

        provider = location.getProvider();


        Double longitude = location.getLongitude();
        Double latitude = location.getLatitude();
        Double altitude = location.getAltitude();


        for(int tid = 0 ; tid < targetList.size() ; tid++){

            distanceList.set(tid, (double) location.distanceTo(targetList.get(tid).getLocation()));
        }

        mini = distanceList.get(0);
        // 比較所有目標距離
        for(int did = 0 ; did < distanceList.size();did++){

            if(distanceList.get(did)< mini){
                mini = distanceList.get(did);
                nid = did;
            }
        }

        // 判斷是復靠近
        if (mini < 170) {
            locationTitle.setText("LocationA");
            StringBuilder sb = new StringBuilder();
            sb.append("Longitude:").append(longitude).append("\n");
            sb.append("Latitude:").append(latitude).append("\n");
            sb.append("Altitude:").append(altitude).append("\n");
            sb.append("distance:").append(mini).append("\n");
            sb.append("Close to ").append(targetList.get(nid).getName());
            locationDisp.setText(sb.toString());
            Log.d(TAG,sb.toString());

        } else {
            locationTitle.setText("LocationA");
            StringBuilder sb = new StringBuilder();
            sb.append("Longitude:").append(longitude).append("\n");
            sb.append("Latitude:").append(latitude).append("\n");
            sb.append("Altitude:").append(altitude).append("\n");
            sb.append("distance").append(mini);
            locationDisp.setText(sb.toString());
            Log.d(TAG,sb.toString());
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        gpsTool.stopGpsUpdate();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();

    }

    @Override
    protected void onResume() {
        super.onResume();
        gpsTool.startGpsUpdate();
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, loaderCallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            loaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();

    }

    @Override
    public void onCameraViewStarted(int width, int height) {

    }

    @Override
    public void onCameraViewStopped() {

    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        return inputFrame.rgba();
    }
}
