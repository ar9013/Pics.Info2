package org.opencv.samples.PicsInfo;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;



public class MainActivity extends Activity {

    private Button btnAR , btnGps, btnAbout ;
    private

    static final int MY_PERMISSIONS_ACCESS_FINE_LOCATION = 2;
    static final int MY_PERMISSIONS_ACCESS_COARSE_LOCATION = 4;

    private static final String TAG = "MainActivity";
    int FINE_LOCATION = 1;
    int OPEN_CAMERA = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkGpsPermission();
        btnAR = (Button) findViewById(R.id.btn_ar);
        btnGps = (Button) findViewById(R.id.btn_GPS);
        btnAbout = (Button) findViewById(R.id.btn_about);

        btnAR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent it2ar = new Intent(MainActivity.this,ARActivity.class);
                startActivity(it2ar);
            }
        });

        btnGps = (Button) findViewById(R.id.btn_GPS);
        btnGps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent it2Gps = new Intent(MainActivity.this,GpsActivity.class);
                startActivity(it2Gps);
            }
        });

        btnAbout = (Button) findViewById(R.id.btn_about);
        btnAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent it2abort = new Intent(MainActivity.this,AboutActivity.class);
                startActivity(it2abort);
            }
        });
    }

    private void checkGpsPermission(){
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},FINE_LOCATION);
        }
    }

    private void checkCameraPermission(){
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA}, OPEN_CAMERA);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == FINE_LOCATION){
            Toast.makeText(this,"Success get FINE_LOCATION permission.",Toast.LENGTH_SHORT).show();
        }

        if(requestCode == OPEN_CAMERA){
            Toast.makeText(this,"Success get OPEN_CAMERA permission.",Toast.LENGTH_SHORT).show();
        }
    }

}
